function Container.isContainer(self)
	return true
end

function Container.removeItem(self,itemid, count)
	if not self:isContainer() then
		return false
	end
	if not count then 
		count = 1 
	end
	if(self:getItemCountById(itemid) < count) then
		return false
	end
	local precisaRemover = count
	for i = self:getSize()-1, 0, -1 do
		local item = self:getItem(i)
		if(item:getId() == itemid) then
			if(item:getCount() >= precisaRemover) then
				item:remove(precisaRemover)
				break
			else
				precisaRemover = precisaRemover - item:getCount()
				item:remove(item:getCount())
			end
		end
	end
	return true
end
