receitasBlacksmith = {
[2642] = {
			{
			itemASerCriadoId = 2643,
			needItems = {
						{id = 5913, count = 1}, -- 1 brown piece of cloth
						},
			skillNeeded = 10,
			gainTries = 2,
			itemType = "Shoes",
			},
			-- 

		},
}


arvores = {
	[2768] = {newId = 3873, galhoId = 5901, skillNeeded = 10, gainTries = 45}
}


local storages = {
	lumberjack = 1200,
	mining = 1201,
	blacksmith = 1202,
}
function Player.isLumberjacking(self)
	if(self:getStorageValue(storages.lumberjack) == 1) then
		return true
	end
	return false
end

function Player.isMining(self)
	if(self:getStorageValue(storages.mining) == 1) then
		return true
	end
	return false
end

function Player.isBlacksmithing(self)
	if(self:getStorageValue(storages.blacksmith) == 1) then
		return true
	end
	return false
end

function Player.startBlacksmith(self)
	self:registerEvent("notMove")
	return self:setStorageValue(storages.blacksmith,1)
end

function Player.startLumberjack(self)
	self:registerEvent("notMove")
	return self:setStorageValue(storages.lumberjack,1)
end

function Player.startMining(self)
	self:registerEvent("notMove")
	return self:setStorageValue(storages.mining,1)
end

function Player.stopBlacksmith(self)
	if(self:isPlayer()) then
		self:setStorageValue(storages.blacksmith,-1)
		self:unregisterEvent("notMove")
		return true
	end
	return false
end


function Player.stopLumberjack(self)
	if(self:isPlayer()) then
		self:setStorageValue(storages.lumberjack,-1)
		self:unregisterEvent("notMove")
		return true
	end
	return false
end


function Player.stopMining(self)
	if(self:isPlayer()) then
		self:setStorageValue(storages.mining,-1)
		self:unregisterEvent("notMove")
		return true
	end
	return false
end