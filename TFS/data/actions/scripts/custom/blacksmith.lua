function onUse(player, item, fromPosition, target, toPosition, isHotkey)
	if toPosition.x == 65535 then -- item is inside player
		return player:sendTextMessage(MESSAGE_STATUS_SMALL, "Please insert the item on top of Anvil.")
	end
	local anvil = Tile(toPosition):getItemById(2555)
	if not anvil then
		return player:sendTextMessage(MESSAGE_STATUS_SMALL, "You are not on Anvil.")
	end
	
	local baseItem = receitasBlacksmith[target.itemid]
	if( not baseItem ) then
		return player:sendTextMessage(MESSAGE_STATUS_SMALL, "You can't craft anything with this base item.")
	end

	local window = ModalWindow {
		title = 'Blacksmithing',
		message = 'Choose an item and press Craft It!'
	}
	

	for i = 1, #baseItem do
		if(player:getEffectiveSkillLevel(SKILL_BLACKSMITH) >= baseItem[i].skillNeeded) then
			for j = 1,#baseItem[i].needItems do
				if (anvil:getItemCountById(baseItem[i].needItems[j].id) < baseItem[i].needItems[j].count) then
					break
				end	
				if(j == #baseItem[i].needItems) then
					local choice = window:addChoice(ItemType(baseItem[i].itemASerCriadoId):getName())
					choice.needItems = {}
					choice.toPos = {}
					choice.baseItemId = target.itemid
					choice.nextItemId = baseItem[i].itemASerCriadoId
					choice.playerId = player:getId()
					choice.gainTries = baseItem[i].gainTries
					choice.toPos.x = toPosition.x
					choice.toPos.y = toPosition.y
					choice.toPos.z = toPosition.z
					for k = 1,#baseItem[i].needItems do
						table.insert(choice.needItems,{id = baseItem[i].needItems[k].id, count = baseItem[i].needItems[k].count})
					end
				end
			end
		end
	end
	
	window:addButton('Craft It!',
    function(button, choice)
		if not choice then
			player:stopBlacksmith()
			return
		end
		
		for i = 1,#choice.needItems do
			if (anvil:getItemCountById(choice.needItems[i].id) < choice.needItems[i].count) then
				player:sendTextMessage(MESSAGE_STATUS_SMALL, "Someone removed some of needed items from you anvil.")
				player:stopBlacksmith()
				break
			end	
			if i == #choice.needItems then
				local itemToTransform = Tile(choice.toPos):getItemById(choice.baseItemId)
				if not itemToTransform then
					player:sendTextMessage(MESSAGE_STATUS_SMALL, "Someone removed the base-item from anvil.")
					player:stopBlacksmith()		
					break
				end
				for j = 1,#choice.needItems do
					anvil:removeItem(choice.needItems[j].id, choice.needItems[j].count)
				end
				itemToTransform:transform(choice.nextItemId)
				player:addSkillTries(SKILL_BLACKSMITH, choice.gainTries)
				local position = Position(choice.toPos)
				position:sendMagicEffect(CONST_ME_MAGIC_GREEN)
				Game.sendAnimatedText(player:getPosition(), "+" .. choice.gainTries, 65)
				player:stopBlacksmith()	
			end
		end		
    end
	)
	-- Set this button as the default enter button
	window:setDefaultEnterButton('Craft It!')

	-- Add a button without a specific callback
	window:addButton('Cancel')
	window:setDefaultEscapeButton('Cancel')

	-- If a button without a specific callback is pressed, this fucntion will be called
	window:setDefaultCallback(
		function(button, choice)
			player:stopBlacksmith()
		end
	)

	player:startBlacksmith()	
	window:sendToPlayer(player)			
	return true

--[[	local function cor(i)
		Game.sendAnimatedText(player:getPosition(), "+" .. i, i )
		if(i < 255) then
			addEvent(cor,500,i+1)
		end
	end
	cor(1)
--]]
end
