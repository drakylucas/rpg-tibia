function cutTree(player,targetId,tries,toPosition,newId,galhoId,gainTries,skill,skillNeeded)
	
	local jogador = Player(player)
	local target = Tile(toPosition):getItemById(targetId)

	if(not target)then
		print("Aparentemente tem dado erros no script lumberjack, de uma olhada nisso!")
		return false
	end

	if( not jogador ) then
		target:setActionId(-1)
		return false
	end
	if(math.random(1,100) <= 5)then
		jogador:removeItem(2386, 1)
		target:setActionId(-1)
		jogador:sendTextMessage(MESSAGE_STATUS_SMALL, "Your axe has broken.")
		jogador:stopLumberjack()
		toPosition:sendMagicEffect(CONST_ME_POFF)
		return false
	end
	if(tries == 2 )then
		local posPlayer = Position(jogador:getPosition())
		posPlayer:sendMagicEffect(CONST_ME_MAGIC_GREEN)
		toPosition:sendMagicEffect(79) -- efeito ao ganhar os galhos
		jogador:addSkillTries(SKILL_LUMBERJACKING, gainTries) -- 8 = SKILL_LUMBERJACKING
		Game.sendAnimatedText(jogador:getPosition(), "+" .. gainTries, 65)
		jogador:addItem(galhoId, math.random(1,math.abs(skill/skillNeeded)))
		jogador:stopLumberjack()
		target:transform(newId)
		target:decay()
		target:setActionId(-1)
		return true
	else
		toPosition:sendMagicEffect(79) -- efeito do axe batendo na arvore
		return addEvent(cutTree,1500,player,targetId,(tries+1),toPosition,newId,galhoId,gainTries,skill,skillNeeded)
	end
end

function onUse(player, item, fromPosition, target, toPosition, isHotkey)
	if not target:isItem() then return false end
	local arvoreASerCortada = arvores[target.itemid]
	
	if(not arvoreASerCortada) then
		return false
	end
	if(target.actionid == 1200) then
		return player:sendTextMessage(MESSAGE_STATUS_SMALL, "Someone is cutting this tree. You can't cut the same tree.")
	end
	
	if(player:isLumberjacking()) then
		return player:sendTextMessage(MESSAGE_STATUS_SMALL, "You are cutting this tree.")
	end
	local skill = player:getEffectiveSkillLevel(SKILL_LUMBERJACKING)
	if(skill < arvoreASerCortada.skillNeeded) then
		return player:sendTextMessage(MESSAGE_STATUS_SMALL, "You need lumberjacking skill ".. arvoreASerCortada.skillNeeded .. " to cut this tree!")
	end
	player:startLumberjack()
	target:setActionId(1200)
	cutTree(player:getId(),target:getId(),0,toPosition,arvoreASerCortada.newId,arvoreASerCortada.galhoId,arvoreASerCortada.gainTries,skill,arvoreASerCortada.skillNeeded)
	return true
end
