blacksmithWindow = nil
blacksmithRecipeWindow = nil
blacksmithButton = nil
blacksmithPanel = nil
function init()

  blacksmithButton = modules.client_topmenu.addRightGameToggleButton('blacksmithButton', tr('Blacksmith Recipes') .. '', '/images/topbuttons/inventory', toggle)
  blacksmithButton:setOn(true)
  blacksmithWindow = g_ui.loadUI('blacksmith', modules.game_interface.getRightPanel())
  blacksmithWindow:disableResize()
  blacksmithPanel = blacksmithWindow:getChildById('blacksmithPanel')

end

function toggle()
  if blacksmithButton:isOn() then
    blacksmithWindow:close()
    inventoryButton:setOn(false)
  else
    blacksmithWindow:open()
    blacksmithButton:setOn(true)
  end
end

function terminate()
  blacksmithWindow:destroy()
  blacksmithButton:destroy()
end
