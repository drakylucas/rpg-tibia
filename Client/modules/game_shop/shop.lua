local shopWindow
local shopButton
local shopTabBar
local addonsPanel
local itemsPanel
local premiumPanel
local mountsPanel
local acceptWindow

local shop_all = {
  ["outfits"] = {
    {name = "Evoker",priceCoins = 10, priceTibiaGold = 100, typeMale = 725,typeFemale = 724},
    {name = "Seaweaver",priceCoins = 10, priceTibiaGold = 100, typeMale = 733,typeFemale = 732},
    {name = "Recruiter",priceCoins = 10, priceTibiaGold = 100, typeMale = 746,typeFemale = 745},
    {name = "Pirate Master",priceCoins = 10, priceTibiaGold = 100, typeMale = 750,typeFemale = 749},
  },
  ["mounts"] = {
    {},
    {},
},
  ["items"] = {
    {},
    {},
  },
  ["VIP"] = {
    {},
    {},
  },
}
--]]

local addonTest
function init()
  connect(g_game, { onGameStart = online,
                    onGameEnd   = hide })
  shopWindow = g_ui.displayUI('shop')
  shopWindow:hide()
  
  shopTabBar = shopWindow:getChildById('shopTabBar')
  shopTabBar:setContentWidget(shopWindow:getChildById('shopTabContent'))

  addonsPanel = g_ui.loadUI('addons')
  shopTabBar:addTab(tr('Addons'), addonsPanel, '/modules/game_shop/images/shoptabs/addons')

  itemsPanel = g_ui.loadUI('items')
  shopTabBar:addTab(tr('Items'), itemsPanel, '/modules/game_shop/images/shoptabs/items')

  mountsPanel = g_ui.loadUI('mounts')
  shopTabBar:addTab(tr('Mounts'), mountsPanel, '/modules/game_shop/images/shoptabs/mounts')

  premiumPanel = g_ui.loadUI('premium')
  shopTabBar:addTab(tr('Premium'), premiumPanel, '/modules/game_shop/images/shoptabs/premium')

  shopButton = modules.client_topmenu.addLeftGameButton('shopButton', tr('Shop'), '/modules/game_shop/images/shop', toggle)
end


-- create shops:
function createOutfitsShop(shop)
  local marginTop = 5
  local marginLeft = 5
  for i = 1,#shop do
    local AddonButton = g_ui.createWidget('AddonButton',addonsPanel)
    AddonButton:setId("AddonButton"..i)
    local outfitMale = AddonButton:recursiveGetChildById('outfitMale')
    local outfitFemale = AddonButton:recursiveGetChildById('outfitFemale')
    local outfitName = AddonButton:recursiveGetChildById('outfitName')
    local priceTibiaCoins = AddonButton:recursiveGetChildById('priceTibiaCoins')
    local priceGold = AddonButton:recursiveGetChildById('priceGold')
    
    outfitName:setText(shop[i].name)
    outfitMale:setOutfit({type = shop[i].typeMale,head = 78,body = 47,legs = 30,feet = 11,addons = 3})
    outfitFemale:setOutfit({type = shop[i].typeFemale,head = 0,body = 94,legs = 0,feet = 65,addons = 3})    
    priceTibiaCoins:setText(shop[i].priceCoins)
    priceGold:setText(shop[i].priceTibiaGold)
    
    
    AddonButton:setMarginLeft(marginLeft)
    AddonButton:setMarginTop(marginTop)
    marginLeft = marginLeft + 102
    if(i%4 == 0) then
      marginTop = marginTop + 126
      marginLeft = 5
    end
  end   
end




-- end create shops




function terminate()
  disconnect(g_game, { onGameStart = online,
                       onGameEnd   = hide })
  shopWindow:destroy()
  shopButton:destroy()
end

function online()
  local player = g_game.getLocalPlayer()
  if not player then return end
  createOutfitsShop(shop_all["outfits"])
end

function toggle()
  if shopWindow:isVisible() then
    hide()
  else
    show()
  end
end

function show()
  shopWindow:show()
  shopWindow:raise()
  shopWindow:focus()
end

function hide()
  shopWindow:hide()
  if acceptWindow then
    acceptWindow:destroy()
    acceptWindow = nil
  end
end

-- Buy functions:

-- Addons
--[[
function buyAssassin()
  if acceptWindow then
    return true
  end

  local acceptFunc = function()
    g_game.talk('!buyaddon assassin')
	acceptWindow:destroy()
	acceptWindow = nil
  end
  
  local cancelFunc = function() acceptWindow:destroy() acceptWindow = nil end

  acceptWindow = displayGeneralBox(tr('Accept transaction'), tr("Do you really want to buy this item?"),
  { { text=tr('Yes'), callback=acceptFunc },
    { text=tr('No'), callback=cancelFunc },
    anchor=AnchorHorizontalCenter }, acceptFunc, cancelFunc)
  return true
end
--]]